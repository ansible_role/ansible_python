## Ansible role for installing [python](https://python.org) from sources.

Python will be installed with */usr/local* prefix. `python_version` variable 
defines version of interpreter, that will be installed.

***NOTE:** if python interpreter exists in /usr/local/bin and it has the same
 version as was declared for installation, role will just update some python 
 packages like pip and virtualenv.*
 